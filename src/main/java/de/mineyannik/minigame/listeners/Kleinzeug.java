/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package de.mineyannik.minigame.listeners;

import de.mineyannik.minigame.Main;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerPreLoginEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.server.ServerListPingEvent;
import org.bukkit.inventory.ItemStack;

/**
 *
 * @author yannikhenke
 */
public class Kleinzeug implements Listener {
    
    @EventHandler
    public void onJoin(PlayerJoinEvent e)
    {
        Player p = e.getPlayer();
        
        p.teleport(p.getWorld().getSpawnLocation());
        
        p.setHealth(20D);
        p.setFoodLevel(20);
        p.setLevel(0);
        
        p.getInventory().clear();
        
        p.getInventory().setItem(0, new ItemStack(Material.CHEST));
        p.getInventory().setItem(8, new ItemStack(Material.SLIME_BALL));
    }
    
    @EventHandler
    public void onPreJoin(AsyncPlayerPreLoginEvent e)
    {
        if (Main.status.equalsIgnoreCase("ready"))
        {
            if (Bukkit.getServer().getMaxPlayers() > Bukkit.getServer().getOnlinePlayers().length)
            {
                e.allow();
            }
            else
            {
                e.disallow(AsyncPlayerPreLoginEvent.Result.KICK_FULL, ChatColor.RED + "Der Server ist voll!");
            }

        }
        else
        {
            e.disallow(AsyncPlayerPreLoginEvent.Result.KICK_OTHER, ChatColor.RED + "Spiel läuft bereits!");
        }
    }
    
    @EventHandler
    public void onPing(ServerListPingEvent e)
    {
        if (Main.status.equalsIgnoreCase("ready"))
        {
            e.setMotd(ChatColor.GREEN + "Lobby");
        }
        else
        {
            e.setMotd(ChatColor.RED + "Running");
        }
    }
    
    @EventHandler
    public void onInteract(PlayerInteractEvent e)
    {
        if (e.getItem() == null)
        {
            return;
        }
        
        if (e.getItem().getType() == Material.SLIME_BALL)
        {
            e.getPlayer().kickPlayer("");
        }
    }
    
}
